'''
    Commons-Category-Scraper
    Copyright (C) 2019 Benjamin Lowry

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''
import mwclient
from PIL import Image
import os

site = mwclient.Site(('https', 'commons.wikimedia.org'))
category = site.pages["Category:" + input("Category:")]
attributions = "Commons-Category-Scraper Attribution Log"

if not os.path.exists("dl/"):
    os.makedirs("dl/")

for page in category:
    if type(page) == mwclient.image.Image:
        if page.imageinfo['width'] < 2000 or page.imageinfo['height'] < 2000:
            print("Skipping")
        else:
            with open("dl/" + page.page_title, 'wb') as file:
                print("Downloading " + page.page_title)
                page.download(file)
            attributions = attributions + "\n" + page.page_title + " from Wikimedia Commons (" + page.imageinfo['descriptionurl'] + ") uploaded by " + page.imageinfo['user']
            print("Cropping...")
            img = Image.open("dl/" + page.page_title)
            img.crop((0, 0, 2000, 2000)).save("dl/crop_" + page.page_title)
with open("dl/attributions.txt", 'wb') as file:
    print("Writing attributions...")
    file.write(str.encode(attributions))